R5py
====

**R5py** is a Python wrapper for the `R5 routing analysis engine <https://github.com/conveyal/r5>`_. It’s inspired by `r5r, a wrapper for R <https://ipeagit.github.io/r5r/>`_, and is designed to interact with `GeoPandas <https://geopandas.org/>`_ data frames.


.. toctree::
   :maxdepth: 1
   :caption: Contents

   installation
   basic-usage.ipynb

.. toctree::
    :caption: Reference

    Module contents <reference>
