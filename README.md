# r5py: run R5 from Python

R5py is a Python wrapper for the [R5 routing analysis engine](https://github.com/conveyal/r5). It’s inspired by [r5r, a wrapper for R](https://ipeagit.github.io/r5r/), and is designed to interact with [GeoPandas](https://geopandas.org/) data frames.


